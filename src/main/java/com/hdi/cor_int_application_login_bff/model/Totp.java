package com.hdi.cor_int_application_login_bff.model;

import javax.validation.constraints.Email;

public class Totp {

    private String cpfCnpj;

    private String login;

    private String phone;

    @Email
    private String email;

    private Integer modoEnvio;

    public Totp(String cpfCnpj, String phone, @Email String email, Integer modoEnvio) {
        this.cpfCnpj = cpfCnpj;
        this.phone = phone;
        this.email = email;
        this.modoEnvio = modoEnvio;
        this.login = cpfCnpj;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public Totp(String login) {
        this.login = login;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getModoEnvio() {
        return modoEnvio;
    }

    public void setModoEnvio(Integer modoEnvio) {
        this.modoEnvio = modoEnvio;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
