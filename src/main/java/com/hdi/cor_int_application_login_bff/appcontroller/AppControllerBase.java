package com.hdi.cor_int_application_login_bff.appcontroller;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

/**
 * Generic mapper to DTOs.
 */
@Component("appControllerBase")
public class AppControllerBase {
	private ModelMapper modelMapper;

	public AppControllerBase() {
		this.modelMapper = new ModelMapper();
	}

	public <S, D> D mapTo(S source, Class<D> dest) {
		return modelMapper.map(source, dest);
	}

}
