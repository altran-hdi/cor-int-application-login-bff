package com.hdi.cor_int_application_login_bff.services;

import com.google.gson.Gson;
import com.hdi.cor_int_application_login_bff.dto.output.LoginOutput;
import com.hdi.cor_int_application_login_bff.dto.output.TokenResponse;
import com.hdi.cor_int_application_login_bff.model.Login;
import com.hdi.cor_int_application_login_bff.model.Totp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class LoginService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginService.class.getSimpleName());

    @Value("${app.cor-int-login-2fa-totp-create.uri.generate}")
    private String URI_GENERATE;

    @Value("${app.cor-int-login-2fa-totp-check.uri.validate}")
    private String URI_VALIDATE;

    @Value("${app.ins-cross-authentication-be.url}")
    private String URL_TOKEN;

    @Value("${app.ins-cross-authentication-be.uri.token}")
    private String URI_TOKEN;

    private final String GRANT_TYPE = "client_credentials";

    @Autowired
    public LoginService() {
    }

    public LoginOutput login(Login login){
        TokenResponse token = getToken(login);
        return new LoginOutput(login.getLogin(), token);
    }

    private TokenResponse getToken(Login login) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        Gson parser = new Gson();

        headers.setBasicAuth(login.getLogin(), login.getPassword());
        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", GRANT_TYPE);

        HttpEntity<LinkedMultiValueMap<String, String>> requestEntity =
                new HttpEntity<>(params, headers);

        ResponseEntity<String> response = restTemplate.postForEntity(URI_TOKEN, requestEntity , String.class);
        return parser.fromJson(response.getBody(), TokenResponse.class);
    }
}
