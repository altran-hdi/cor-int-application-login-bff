package com.hdi.cor_int_application_login_bff.services;

import com.google.gson.Gson;
import com.hdi.cor_int_application_login_bff.dto.input.TotpCheckInput;
import com.hdi.cor_int_application_login_bff.dto.output.LoginOutput;
import com.hdi.cor_int_application_login_bff.dto.output.TokenResponse;
import com.hdi.cor_int_application_login_bff.dto.output.TotpOutput;
import com.hdi.cor_int_application_login_bff.model.Login;
import com.hdi.cor_int_application_login_bff.model.Totp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.CacheRequest;

@Service
public class TotpService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TotpService.class.getSimpleName());

    @Value("${app.cor-int-login-2fa-totp-create.uri.generate}")
    private String URI_GENERATE;

    @Value("${app.cor-int-login-2fa-totp-check.uri.validate}")
    private String URI_VALIDATE;

    @Value("${app.ins-cross-authentication-be.url}")
    private String URL_TOKEN;

    @Value("${app.ins-cross-authentication-be.uri.token}")
    private String URI_TOKEN;

    private final String GRANT_TYPE = "client_credentials";

    @Autowired
    public TotpService() {
    }

    public void generateTotpCode(Totp totp) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.postForEntity(URI_GENERATE, totp, TotpOutput.class);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    public ResponseEntity<TotpOutput> validateCode(TotpCheckInput totpCheckInput){
        TotpOutput totpOutput = validateTotpCode(totpCheckInput);
        ResponseEntity<TotpOutput> response;
        if(totpOutput != null) {
             response = ResponseEntity.ok(totpOutput);
        } else {
            response = ResponseEntity.status(401).body(null);
        }

        return response;
    }

    private TotpOutput validateTotpCode(TotpCheckInput totp) {
        try {
            Gson parser = new Gson();
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(URI_VALIDATE, totp, String.class);
            return parser.fromJson(responseEntity.getBody(), TotpOutput.class);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }
}
