package com.hdi.cor_int_application_login_bff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CorIntApplicationLoginBff {

    public static void main(String[] args) {
        SpringApplication.run(CorIntApplicationLoginBff.class, args);
    }

}
