package com.hdi.cor_int_application_login_bff.controllers;

import com.hdi.cor_int_application_login_bff.dto.IO.TotpIO;
import com.hdi.cor_int_application_login_bff.dto.input.TotpCheckInput;
import com.hdi.cor_int_application_login_bff.dto.input.TotpInput;
import com.hdi.cor_int_application_login_bff.dto.output.TotpOutput;
import com.hdi.cor_int_application_login_bff.model.Totp;
import com.hdi.cor_int_application_login_bff.services.TotpService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.net.URI;

@RestController
@RequestMapping(path = "/totp")
@Api(tags = "Totp")
@CrossOrigin
public class TotpController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TotpController.class.getSimpleName());

    private TotpIO totpIO;
    private TotpService totpService;

    @Autowired
    public TotpController(TotpIO totpIO, TotpService totpService) {
        this.totpIO = totpIO;
        this.totpService = totpService;
    }

    @PostMapping({"", ""})
    @ApiOperation(value = "Send Totp")
    public void totpSend(@Valid @RequestBody TotpInput totpInput) throws UnsupportedEncodingException {
        Totp totp = totpIO.mapTo(totpInput);
        LOGGER.info("trying generate Totp code with " + totpInput.getLogin());
        totpService.generateTotpCode(totp);
        //@formatter:off
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(totpInput.getLogin())
                .toUri();
        LOGGER.info("TOTP code to" + totpInput.getLogin() + " created at " + location);
        //@formatter:on
    }

    @PostMapping({"/validate-totp", "/validate-totp"})
    @ApiOperation(value = "2FA")
    public ResponseEntity<Object> totpCheck(@Valid @RequestBody TotpCheckInput totpCheckInput) {
        LOGGER.info("trying check Totp with " + totpCheckInput.getLogin());
        ResponseEntity check2FA = totpService.validateCode(totpCheckInput);
        //@formatter:off
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(totpCheckInput.getLogin())
                .toUri();
        LOGGER.info("TOTP code to" + totpCheckInput.getLogin() + " validated at " + location);
        return check2FA;
        //@formatter:on
    }
}
