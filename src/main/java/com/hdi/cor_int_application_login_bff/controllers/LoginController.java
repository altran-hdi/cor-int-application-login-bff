package com.hdi.cor_int_application_login_bff.controllers;

import com.hdi.cor_int_application_login_bff.dto.IO.LoginIO;
import com.hdi.cor_int_application_login_bff.dto.input.LoginInput;
import com.hdi.cor_int_application_login_bff.dto.input.TotpInput;
import com.hdi.cor_int_application_login_bff.dto.output.LoginOutput;
import com.hdi.cor_int_application_login_bff.model.Login;
import com.hdi.cor_int_application_login_bff.model.Totp;
import com.hdi.cor_int_application_login_bff.services.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.net.URI;

@RestController
@RequestMapping(path = "/login")
@Api(tags = "Login")
@CrossOrigin
public class LoginController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class.getSimpleName());

    private LoginIO loginIO;
    private LoginService loginService;

    @Autowired
    public LoginController(LoginIO loginIO, LoginService loginService) {
        this.loginIO = loginIO;
        this.loginService = loginService;
    }

    @PostMapping({"", ""})
    @ApiOperation(value = "Login User")
    public LoginOutput loginUser(@Valid @RequestBody LoginInput loginInput) throws UnsupportedEncodingException {
        Login login = loginIO.mapTo(loginInput);
        LOGGER.info("trying login with " + loginInput.getLogin());
        LoginOutput loginOutput = loginService.login(login);
        //@formatter:off
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(loginOutput.getLogin())
                .toUri();
        LOGGER.info("TOTP code to" + loginOutput.getLogin() + " created at " + location);
        return loginOutput;
        //@formatter:on
    }

    @PostMapping({"/validate-totp", "/validate-totp"})
    @ApiOperation(value = "2FA")
    public ResponseEntity<Object> totpCheck(@Valid @RequestBody TotpInput totpInput) {
        LOGGER.info("trying login with " + totpInput.getLogin());
        Totp check2FA = loginService.validateCode(totpInput.getLogin(), totpInput.getTotpCode());
        //@formatter:off
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(totpInput.getLogin())
                .toUri();
        LOGGER.info("TOTP code to" + totpInput.getLogin() + " validated at " + location);
        return ResponseEntity.ok(location);
        //@formatter:on
    }
}
