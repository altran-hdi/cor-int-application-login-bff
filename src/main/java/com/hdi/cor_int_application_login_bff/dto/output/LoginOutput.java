package com.hdi.cor_int_application_login_bff.dto.output;

public class LoginOutput {

    private String login;
    private String token;
    private String tokenType;
    private Long expiresIn;
    private String refreshToken;
    private String scope;

    public LoginOutput() {
    }

    public LoginOutput(String login, TokenResponse tokenResponse){
        this.login = login;
        this.token = tokenResponse.getAccess_token();
        this.tokenType = tokenResponse.getToken_type();
        this.expiresIn = tokenResponse.getExpires_in();
        this.refreshToken = tokenResponse.getRefresh_token();
        this.scope = tokenResponse.getScope();
    }

    public LoginOutput(String login, String token, String tokenType, Long expiresIn, String refreshToken, String scope) {
        this.login = login;
        this.token = token;
        this.tokenType = tokenType;
        this.expiresIn = expiresIn;
        this.refreshToken = refreshToken;
        this.scope = scope;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public Long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}