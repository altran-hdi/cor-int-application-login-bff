package com.hdi.cor_int_application_login_bff.dto.output;

public class TotpOutput {

    private String status;



    public TotpOutput() {
    }

    public TotpOutput(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}