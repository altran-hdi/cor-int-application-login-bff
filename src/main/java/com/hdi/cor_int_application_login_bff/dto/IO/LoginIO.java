package com.hdi.cor_int_application_login_bff.dto.IO;

import com.hdi.cor_int_application_login_bff.dto.input.LoginInput;
import com.hdi.cor_int_application_login_bff.dto.output.LoginOutput;
import com.hdi.cor_int_application_login_bff.model.Login;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

@Component("LoginIO")
public class LoginIO {
        private ModelMapper modelMapper;

        final Converter<LoginInput, Login> loginConverter = new Converter<LoginInput, Login>() {
            @Override
            public Login convert(MappingContext<LoginInput, Login> context) {
                LoginInput loginInput = context.getSource();
                // @formatter:off

                //TODO: Aplicar crypto
                return new Login(loginInput.getCpfCnpj(), loginInput.getPassword());
                // @formatter:on
            }
        };

    final Converter<Login, LoginOutput> loginOutputConverter = new Converter<Login, LoginOutput>() {
        @Override
        public LoginOutput convert(MappingContext<Login, LoginOutput> context) {
            Login login = context.getSource();
            // @formatter:off


            return new LoginOutput();
            // @formatter:on
        }
    };
        public LoginIO() {
            modelMapper = new ModelMapper();
            modelMapper.addConverter(loginConverter);
        }

        public Login mapTo(LoginInput loginInput) {
            return this.modelMapper.map(loginInput, Login.class);
        }
}
