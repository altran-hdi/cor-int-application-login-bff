package com.hdi.cor_int_application_login_bff.dto.IO;

import com.hdi.cor_int_application_login_bff.dto.input.TotpInput;
import com.hdi.cor_int_application_login_bff.model.Totp;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

@Component("TotpIO")
public class TotpIO {
        private ModelMapper modelMapper;

        final Converter<TotpInput, Totp> totpConverter = new Converter<TotpInput, Totp>() {
            @Override
            public Totp convert(MappingContext<TotpInput, Totp> context) {
                TotpInput totpInput = context.getSource();
                // @formatter:off

                //TODO: Aplicar crypto
                return new Totp(totpInput.getLogin(), totpInput.getPhone(), totpInput.getEmail(), totpInput.getModoEnvio());
                // @formatter:on
            }
        };

        public TotpIO() {
            modelMapper = new ModelMapper();
            modelMapper.addConverter(totpConverter);
        }

        public Totp mapTo(TotpInput totpInput) {
            return this.modelMapper.map(totpInput, Totp.class);
        }
}
