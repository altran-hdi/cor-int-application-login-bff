package com.hdi.cor_int_application_login_bff.dto.input;

import io.swagger.annotations.ApiModelProperty;

public class TotpCheckInput {

    @ApiModelProperty(example = "joseSilva")
    private String login;

    @ApiModelProperty(example = "123456")
    private Integer totpCode;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getTotpCode() {
        return totpCode;
    }

    public void setTotpCode(Integer totpCode) {
        this.totpCode = totpCode;
    }
}
