package com.hdi.cor_int_application_login_bff.dto.input;

import io.swagger.annotations.ApiModelProperty;

public class TotpInput {

    @ApiModelProperty(example = "joseSilva")
    private String login;

    @ApiModelProperty(example = "(83)99992-0047")
    private String phone;

    @ApiModelProperty(example = "teste@teste.com")
    private String email;

    @ApiModelProperty(example = "0")
    private Integer modoEnvio;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getModoEnvio() {
        return modoEnvio;
    }

    public void setModoEnvio(Integer modoEnvio) {
        this.modoEnvio = modoEnvio;
    }
}
