package com.hdi.cor_int_application_login_bff.dto.input;

import io.swagger.annotations.ApiModelProperty;

public class LoginInput {

    @ApiModelProperty(example = "joseSilva")
    private String cpfCnpj;

    @ApiModelProperty(example = "abc123")
    private String password;

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
